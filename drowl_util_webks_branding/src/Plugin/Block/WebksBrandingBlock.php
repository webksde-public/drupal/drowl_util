<?php

namespace Drupal\drowl_util_webks_branding\Plugin\Block;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'DROWL.de Branding' Block.
 *
 * @Block(
 *   id = "drowl_util_webks_branding",
 *   admin_label = @Translation("DROWL.de Branding"),
 *   category = @Translation("DROWL.de"),
 * )
 */
class WebksBrandingBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The module extension list service.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * Constructs a new WebksBrandingBlock instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Extension\ModuleExtensionList $moduleExtensionList
   *   The module extension list service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ModuleExtensionList $moduleExtensionList) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->moduleExtensionList = $moduleExtensionList;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('extension.list.module')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#markup' => new FormattableMarkup("<div class='developer-brand developer-brand--drowl'><a href='http://www.drowl.de' target='_blank'><img class='developer-brand__logo' src=':imageSrc' alt='websolutions kept simple - webbasierte L&ouml;sungen die einfach &uuml;berzeugen' /></a></div>", [
        ':imageSrc' => $this->moduleExtensionList->getPath('drowl_util_webks_branding') . '/img/drowl_logo_small_banner.svg',
      ]),
    ];
  }

}
